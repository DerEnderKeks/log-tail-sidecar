#!/usr/bin/env bash

files=()
dirs=()

usage() {
  echo "Usage: $0 [-w <directory>] [<-d <directory> | -f <file>>...]"
  echo "  -w <directory> working directory (base path)"
  echo "  -d <directory> Directory to recursively search for files to tail"
  echo "  -f <file>      File to tail"
  echo ""
  echo "-f and -d can be specified multiple times."
  exit 22
}

while getopts ":w:f:d:" o; do
  case "${o}" in
  w)
    wd="$OPTARG"
    ;;
  f)
    files+=("$OPTARG")
    ;;
  d)
    dirs+=("$OPTARG")
    ;;
  *)
    usage
    ;;
  esac
done
shift $((OPTIND - 1))

cd "${wd:-/logs}" || exit 1

if [ ${#files[@]} -eq 0 ] && [ ${#dirs[@]} -eq 0 ]; then
  dirs=(".") # default to working directory
fi

for dir in "${dirs[@]}"; do
  if [ ! -d "$dir" ]; then
    echo "Directory $dir does not exist"
    exit 1
  fi

  readarray -d '' new_files < <(find "$dir" -type f -print0)
  files+=("${new_files[@]}")
done

if [ ${#files[@]} -eq 0 ]; then
  echo "No files found"
  exit 1
fi

exec parallel --tagstring "{}:" --line-buffer tail -n 0 -f {} ::: "${files[@]}"
