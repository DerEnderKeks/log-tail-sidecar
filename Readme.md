# Log Tail Sidecar

This container image is meant to be used as a sidecar to print log files to stdout.

## Usage

Include the following in your Kubernetes manifest:

```yaml
containers:
  - name: log-reader
    image: registry.gitlab.com/derenderkeks/log-tail-sidecar:main
    args: [] # specify custom options here
    volumeMounts:
      - mountPath: /logs
        readOnly: true
        name: logs # Name of the volume containing log files
```

By default, all files in `/logs` will be tailed. You can use the following options as `args` to modify the behavior:

```
Usage: /entrypoint.sh [-w <directory>] [<-d <directory> | -f <file>>...]
  -w <directory> working directory (base path)
  -d <directory> Directory to recursively search for files to tail
  -f <file>      File to tail

-f and -d can be specified multiple times.
```

Logs will be printed to the stdout of the sidecar in the following format:

```
<filename>:<tab><line>
```

## License

See [License](License).