FROM alpine:3.16.0
LABEL maintainer="DerEnderKeks"

RUN apk add --no-cache parallel findutils coreutils bash && \
    mkdir ~/.parallel && touch ~/.parallel/will-cite && \
    mkdir /logs

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]